#%%
'''
This Pyton script presents the modal analysis performed for the study of microperforated plates under an acoustic nonlinearity regime. 
The equations is presented in [1].

In this script, a resolution by the harmonic balancing method is performed.
Here, only one harmonic and one mode are studied.


[1] : Lucie Gallerand, Mathias Legrand, Raymond Panneton, Philippe Leclaire​, Thomas Dupont. 
Added nonlinear damping of homogenized fluid-saturated microperforated plates in Forchheimer flow regime. 2024. 
https://hal.science/hal-04528668
'''
#%% --------- Imported packadge -----------
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad, nquad
from scipy.optimize import fsolve
import numba

from scipy.optimize import newton

#%%
"""
Parameter dictionary

All necessary parameters are stored in the following dictionary. They are divided into 5 categories: 
    
    [a]: the number of dof in the discretization on plate modes and boundary conditions (S: simply supported, C: emasculated and F: free)
    [b]: the geometrical parameters of the structure;
    [c]: perforation parameters (diameter and perforation ratio);
    [d]: the mechanical parameters of the plate and the fluid parameters;
    [e]: frequency discretization.
    
All parameters are given in SI units.
"""
param = {'m': np.array([1]), # number of mode in the x direction
         'n': np.array([1]), # number of mode in the y direction
         'clx' : 'SS',         # Boundary condition in the x direction
         'cly' : 'SS',         # Boundary condition in the y direction
         'index_m' : 0,           # mode of interest in x-direction
         'index_n' : 0,           # mode of interest in y-direction
         #
         'a': 490e-3,          # plate length in the x direction (m)
         'b': 570e-3,          # plate length in the y direction (m)
         'h': 1e-3,            # plate thickness (in the z direction) (m)
         #
         'phi':0.1,            # perforation ratio
         'd':2.2e-3,           # perforation diameter
         #
         'rhof': 1.213,        # fluid density (kg/m3)
         'eta': 1.839e-05,       # fluid dynamic viscosity (Pa/s)
         'rhos': 2680,         # solid density (kg/m3)
         'E': 66000000000,     # solid Young modulus (Pa)
         'nu': 0.33,           # Poisson coefficient
         'Bf': 1.3e5,          # fluid Blunck mudulus (Pa)
         'Fext': 0.03,         # excitation magnitude (N)    
         #
         'w_min':114,           #initial forcing pulsation (rad/s)
         'w_max':116,           #final forcing pulsation   (rad/s)
         'npoint':100,          # number of point in frequency discretization
         'nFext':1}             # number of point in force magnitude discretization

#%%
""" ---------- Recovery and assignment of dictionary parameters ----------- """
m = param['m']; clx = param['clx']; n = param['n']; cly = param['cly']
lx = param['a']; ly = param['b']; h = param['h']
phi = param['phi']; d = param['d']; 
rhof = param['rhof']; rhos = param['rhos']; eta = param['eta']; E = param['E']; nu = param['nu']; Bf = param['Bf']

Fext = param['Fext']
index_m = param['index_m']; index_n = param['index_n']

w_min = param['w_min']; w_max = param['w_max']; npoint = param['npoint']
#%% --- JCA parameters
sigma = 32*eta/(phi*d**2);    #static airflow resistivity
beta = 1.6; Cd = 0.76;            #discharge coefficient (see Laly et al. 2017 article)
e = beta*rhof*(1-phi**2)/(np.pi*h*phi*Cd**2)*1/sigma*phi #Forchheimer parameter
epsilon_1 = 0.48*np.sqrt(np.pi*(d/2)**2)*(1-1.14*np.sqrt(phi)) #end-length correction
alpha_cor1 = 1+2*epsilon_1/h   #corrected tortuosity


#%% --- Mechanical parameters
# E = E*(1-phi)**2/(1+(2-3*nu)*phi)       # corrected Young modulus
rho = phi*rhof+(1-phi)*rhos             # density of the fluid-solid mixture
D = E*h**3/12/(1-nu**2)                 #plate bending
#%%
"""
Calculation of mode shape according x or y direction via the function mode_shape(k,clk,lk)
Input: mode index, boudary condition, plate length

Output: Omega_k and the coefficients ck,dk,ek,fk
"""
def modal_shape(m,cl,a):    
    def f(x):
        if cl == 'SS':
            return np.sin(x)
        if cl == 'CC' or cl == 'FF':
            return np.cos(x)*np.cosh(x)-1            
        if cl == 'FC' or cl == 'CF':
            return np.cos(x)*np.cosh(x)+1
        else:
            return np.tan(x)-np.tanh(x)      
        
    x0 = np.empty_like(m,dtype=float)    
    for i in range(0,m.size):
        if cl=='CC' or cl == 'FF':
            x = (m[i]+1)*np.pi
        else:
            x = m[i]*np.pi
        
        x0[i] = newton(f, x, fprime=None, args=(), tol=1.48e-14, maxiter=50, fprime2=None)
    if cl == 'FF':
        i = m.size - 1
        while i-2 >= 0:
            x0[i] = x0[i-2]
            i -= 1
        x0[0] = 0; x0[1] = 0;
        
    def constantes(cl,ka):
        if cl == 'SS':
            c = 0; d = 0; e = np.zeros_like(ka,dtype = np.float64); f = np.ones_like(ka,dtype=np.float64)
        elif cl == 'CC':
            c = 1; d = -c; e = -(np.cosh(ka)-np.cos(ka))/(np.sinh(ka)-np.sin(ka)); f = -e
        elif cl == 'CF' or cl == 'FC':
            c = 1; d = -c; e = -(np.cosh(ka)+np.cos(ka))/(np.sinh(ka)+np.sin(ka)); f = -e
        elif cl == 'SC' or cl == 'CS':
            c = 1; d = c; e = -(np.sinh(ka)-np.sin(ka))/(np.cosh(ka)+np.cos(ka)); f = e
        elif cl == 'FS' or cl == 'SF':
            c = 1; d = -c; e = -(np.cosh(ka)-np.cos(ka))/(np.sinh(ka)-np.sin(ka)); f = -e
        elif cl == 'FF':
            c = 1; d = c; e = np.empty_like(ka,dtype = np.float64); e[:2] = 0; e[2:] = -(np.cosh(ka[2:]) - np.cos(ka[2:]))/(np.sinh(ka[2:]) - np.sin(ka[2:])); f = e
                        
        return c, d, e, f
    
    c, d, e, f = constantes(cl, x0)
    k = x0/a  
    return k, c, d, e, f

Omega_m, cx, dx, ex, fx = modal_shape(m, clx, lx) # in x direction
Omega_n, cy, dy, ey, fy = modal_shape(n, cly, ly) # in y direction
 
#%%
"""
Each function calculated the derivative according x or y: 
    modeshape(k,Omega_k,ck,dk,ek,fk) : mode shape according the x or y direction
    modeshape2(k,Omega_k,ck,dk,ek,fk) : the second order derivative of modeshape(k,Omega_k,ck,dk,ek,fk) according to k
    modeshape4(k,Omega_k,ck,dk,ek,fk) : the fourth order derivative of modeshape(k,Omega_k,ck,dk,ek,fk) according to k
"""
def modeshape(k,Omega_k,ck,dk,ek,fk):
    return(ck*np.cosh(Omega_k*k) + dk*np.cos(Omega_k*k) + ek*np.sinh(Omega_k*k) +fk*np.sin(Omega_k*k))
def modeshaped2(k,Omega_k,ck,dk,ek,fk):
    return(Omega_k**2*(ck*np.cosh(Omega_k*k)-dk*np.cos(Omega_k*k)+ek*np.sinh(Omega_k*k)-fk*np.sin(Omega_k*k)))
def modeshaped4(k,Omega_k,ck,dk,ek,fk):
    return((Omega_k**4)*modeshape(k,Omega_k,ck,dk,ek,fk))
"""
Each function calculated the spacial integration, depending of spacial perforation ratio, due to the projection of plate equations on the non-perforated plate modes

    intd4(x, y,Omega_m,cx,dx,ex,fx,kn,cy,dy,ey,fy): integration of D(x,y) × ∇^4 Ψ(x,y) × transpose(Ψ(x,y))
    intd2(x, y,Omega_m,cx,dx,ex,fx,kn,cy,dy,ey,fy): integration of  ∇^2 Ψ(x,y) × transpose(Ψ(x,y))
    intd1(x, y,Omega_m,cx,dx,ex,fx,kn,cy,dy,ey,fy): integration of Ψ(x,y) × transpose(Ψ(x,y))    
    intdfnl(x, y,Omega_m,cx,dx,ex,fx,kn,cy,dy,ey,fy): integration of Ψ(x,y) × |Ψ(x,y)| -- Only valid in the single-mode assumption    

"""
def intd4(x, y,Omega_m,cx,dx,ex,fx,Omega_n,cy,dy,ey,fy):
    return(modeshaped4(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(y,Omega_n,cy,dy,ey,fy)*modeshape(y,Omega_n,cy,dy,ey,fy)
           + 2*modeshaped2(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshaped2(y,Omega_n,cy,dy,ey,fy)*modeshape(y,Omega_n,cy,dy,ey,fy)
           + modeshaped4(y,Omega_n,cy,dy,ey,fy)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(y,Omega_n,cy,dy,ey,fy))
def intd2(x, y,Omega_m,cx,dx,ex,fx,Omega_n,cy,dy,ey,fy):
    return(modeshaped2(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(y,Omega_n,cy,dy,ey,fy)*modeshape(y,Omega_n,cy,dy,ey,fy)
           +modeshaped2(y,Omega_n,cy,dy,ey,fy)*modeshape(y,Omega_n,cy,dy,ey,fy)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx))
def intd1(x,y,Omega_m,cx,dx,ex,fx,Omega_n,cy,dy,ey,fy):
    return(modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(y,Omega_n,cy,dy,ey,fy)*modeshape(y,Omega_n,cy,dy,ey,fy))
def intdfnl(x,y,Omega_m,cx,dx,ex,fx,Omega_n,cy,dy,ey,fy):
    return(modeshape(x,Omega_m,cx,dx,ex,fx)**2*modeshape(y,Omega_n,cy,dy,ey,fy)**2*abs(modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(y,Omega_n,cy,dy,ey,fy)))
#%% ------- Matrix of spacial discretization ------------ 

IM1 = np.empty((m.size,n.size));IM2 = np.empty_like(IM1);I2I6_phi = np.empty_like(IM1);
I2I6 = np.empty_like(IM1); I2I6bis = np.empty_like(IM1);
for i in range(m.size):
    for j in range(n.size):   
        IM1[i,j] = nquad(intd4, [[0, lx], [0, ly]], args=(Omega_m[i],cx,dx,ex[i],fx[i],Omega_n[j],cy,dy,ey[j],fy[j],),opts={"points": [-5, 5],'limit' : 5000000})[0]
        IM2[i,j] = nquad(intd2, [[0, lx], [0, ly]], args=(Omega_m[i],cx,dx,ex[i],fx[i],Omega_n[j],cy,dy,ey[j],fy[j],),opts={"points": [-5, 5],'limit' : 5000000})[0]
        I2I6[i,j] = nquad(intd1, [[0, lx], [0, ly]], args=(Omega_m[i],cx,dx,ex[i],fx[i],Omega_n[j],cy,dy,ey[j],fy[j],),opts={"points": [-5, 5],'limit' : 5000000})[0]
        I2I6bis[i,j] = nquad(intdfnl, [[0, lx], [0, ly]], args=(Omega_m[i],cx,dx,ex[i],fx[i],Omega_n[j],cy,dy,ey[j],fy[j],),opts={"points": [-5, 5],'limit' : 5000000})[0]
#%% ----- Computation of stiffness, mass and damping matrices --------------
ms1 = h*rho*I2I6; ms2 = rhof*I2I6*h
m1 = rhof*h*I2I6; 

k1 = D*IM1; k2 = IM2*h*Bf;

m2 = rhof*I2I6*alpha_cor1/phi*h
cL = sigma*I2I6*h
cNL1 = sigma*e*I2I6bis*h

#%% --------- Measurement point -----------
X0 = lx/10; Y0 = ly/10
#%% Forcing excitation punctual
Fx = cx*np.cosh(Omega_m[index_m]*X0)+dx*np.cos(Omega_m[index_m]*X0)+ex[index_m]*np.sinh(Omega_m[index_m]*X0)+fx[index_m]*np.sin(Omega_m[index_m]*X0)
Fy = cy*np.cosh(Omega_n[index_n]*Y0)+dy*np.cos(Omega_n[index_n]*Y0)+ey[index_n]*np.sinh(Omega_n[index_n]*Y0)+fy[index_n]*np.sin(Omega_n[index_n]*Y0)
Qmat = Fx*Fy
#%%---------------  HBM Calculating harmonic integrals for Nh = 1 ------------------

def integrandcosbis(t, c, d, w, w2):
    return w/np.pi*(c*np.cos(w*t)+d*np.sin(w*t))*abs(c*np.cos(w*t)+d*np.sin(w*t))*np.cos(w*t)
def integrandsinbis(t, c, d, w, w2):
    return w/np.pi*(c*np.cos(w*t)+d*np.sin(w*t))*abs(c*np.cos(w*t)+d*np.sin(w*t))*np.sin(w*t)

#%% ------------------ cost function
"""
the cost function must be equal to zero to ensure convergence.
input: harmonic coefficient
output: vector of 4 functions (2 equations -> cos(wt), sin(wt))  : 
        a) equation 1st componant cos(wt)
        b) equation 1st componant sin(wt)
        c) equation 2nd componant cos(wt)
        d) equation 2nd componant sin(wt)
"""
def fonction(x):
    a,b,c,d = x
    
    c_nl = quad(integrandcosbis, 0, 2*np.pi/w, args=(c,d,w,w2),epsabs=1.49e-5, epsrel=1.49e-12,limit=5000000)[0]#,full_output=0, epsabs=1.49e-08, epsrel=1.49e-08, limit=50, points=None, weight=None, wvar=None, wopts=None, maxp1=50, limlst=50)
    d_nl = quad(integrandsinbis, 0, 2*np.pi/w, args=(c,d,w,w2),epsabs=1.49e-5, epsrel=1.49e-12,limit=5000000)[0]
   
    f1 = k1[index_m,index_n]*a - w2*ms1[index_m,index_n]*a + w*m1[index_m,index_n]*d - Fext*Qmat
    f2 = k1[index_m,index_n]*b - w2*ms1[index_m,index_n]*b - w*m1[index_m,index_n]*c 
    f3 = k2[index_m,index_n]*a - w2*ms2[index_m,index_n]*a + w*m2[index_m,index_n]*d + cL[index_m,index_n]*c +  cNL1[index_m,index_n]*(c_nl)
    f4 = k2[index_m,index_n]*b - w2*ms2[index_m,index_n]*b - w*m2[index_m,index_n]*c + cL[index_m,index_n]*d +  cNL1[index_m,index_n]*(d_nl)
    return(np.array([f1,f2,f3,f4]))
#%% ---------- Results -----------
omega=np.linspace(w_min,w_max,npoint); #vector pulsation 
F = np.linspace(0.02, 5,1)
save=np.zeros((len(omega),2)); #save vector
saveF=np.zeros((len(F),2))
x0 = np.array([0,0,0,0]) #initial guess

#------ iterative resolution loop ------
for k in range(len(F)):
    for idx in range(len(omega)):
        w=omega[idx];w2=w**2; 
        Fext = F[k]             
        temp = fsolve(fonction, x0,xtol = 1e-5,maxfev=500000) #newton raphson type algorithme
        x0 = temp
        save[idx,0]= (np.sqrt(abs(temp[2])**2+abs(temp[3])**2))/Fext #normalized plate displacement
    saveF[k,0] = (np.sqrt(abs(temp[2])**2+abs(temp[3])**2))
Asave = np.zeros((omega.size,2));Asave[:,0]=omega/114.79;Asave[:,1]=save[:,0]*1e2;
plt.plot(omega,save[:,0]);plt.grid() #final plot
plt.xlabel('Forcing frequency (rad/s)'); plt.ylabel('Normalized solid displacement (m/N)')