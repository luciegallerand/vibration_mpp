# -*- coding: utf-8 -*-
"""
This Pyton script presents the modal analysis performed for the study of microperforated plates with multiple perforation diameters.

In this script, the modal analysis in the case of an MPP with two perforation groups with two perforation diameters is proposed. 
The two subdomains are denoted 1 and 2, a perforation diameter and a perforation ratio are associated with each of them.

"""
#%%

"""
  Import necessary packages
"""

import numpy as np  
from numpy.linalg import inv
from scipy.integrate import quad_vec
from scipy import linalg

from mode_shape import mode_shape
#%%
"""
Parameter dictionary

All necessary parameters are stored in the following dictionary. They are divided into 4 categories: 
    
    [1]: the number of dof in the discretization on plate modes and boundary conditions (S: simply supported, C: emasculated and F: free)
    [2]: the geometrical parameters of the structure;
    [3]: perforation parameters (diameter and perforation ratio);
    [4]: the mechanical parameters of the plate and the fluid parameters.
    
All parameters are given in SI units.
"""
param = {'m': np.array([1,2]), # number of mode in the x direction
         'n': np.array([1,2]), # number of mode in the y direction
         'clx' : 'SS',         # Boundary condition in the x direction
         'cly' : 'SS',         # Boundary condition in the y direction
         #
         'lx': 490e-3,          # plate length in the x direction (m)
         'ly': 570e-3,          # plate length in the y direction (m)
         'h': 1e-3,            # plate thickness (in the z direction) (m)
         #
         'phi1':0.05,          # perforation ratio on subdomain 1
         'phi2':0.06,          # perforation ratio on subdomain 2
         'd1':1.4e-3,          # perforation diameter on subdomain 1 (m)
         'd2':1.1e-3,          # perforation diameter on subdomain 2 (m)
         #
         'rhof': 1.213,        # fluid density (kg/m3)
         'eta': 1.84e-5,       # fluid dynamic viscosity (Pa/s)
         'rhos': 2680,         # solid density (kg/m3)
         'E': 66000000000,     # solid Young modulus (Pa)
         'nu': 0.33,           # Poisson coefficient
         'Bf': 1.3e5}          # fluid Blunck mudulus (Pa)
#%%
""" ---------- Recovery and assignment of dictionary parameters ----------- """
m = param['m']; clx = param['clx']; n = param['n']; cly = param['cly']
lx = param['lx']; ly = param['ly']; h = param['h']
d1 = param['d1']; phi1 = param['phi1']; d2 = param['d2']; phi2 = param['phi2']; 
rhof = param['rhof']; rhos = param['rhos']; eta = param['eta']; E = param['E']; nu = param['nu']; Bf = param['Bf']
#%%
"""
Calculation of mode shape according x or y direction via the function mode_shape(k,clk,lk)
Input: mode index, boudary condition, plate length

Output: Omega_k and the coefficients ck,dk,ek,fk
"""
Omega_m, cx, dx, ex, fx = mode_shape(m, clx, lx) # in x direction
Omega_n, cy, dy, ey, fy = mode_shape(n, cly, ly) # in y direction

#%%
"""
Each function calculated the derivative according x or y: 
    modeshape(k,Omega_k,ck,dk,ek,fk) : mode shape according the x or y direction
    modeshape2(k,Omega_k,ck,dk,ek,fk) : the second order derivative of modeshape(k,Omega_k,ck,dk,ek,fk) according to k
    modeshape4(k,Omega_k,ck,dk,ek,fk) : the fourth order derivative of modeshape(k,Omega_k,ck,dk,ek,fk) according to k
"""
def modeshape(k,Omega_k,ck,dk,ek,fk):
    return(ck*np.cosh(Omega_k*k) + dk*np.cos(Omega_k*k) + ek*np.sinh(Omega_k*k) +fk*np.sin(Omega_k*k))
def modeshaped2(k,Omega_k,ck,dk,ek,fk):
    return(Omega_k**2*(ck*np.cosh(Omega_k*k)-dk*np.cos(Omega_k*k)+ek*np.sinh(Omega_k*k)-fk*np.sin(Omega_k*k)))
def modeshaped4(k,Omega_k,ck,dk,ek,fk):
    return((Omega_k**4)*modeshape(k,Omega_k,ck,dk,ek,fk))
"""
Each function calculated the spacial integration due to the projection of plate equations on the non-perforated plate modes
    intd4(k,Omega_k,ck,dk,ek,fk): integration of modeshaped4(k,Omega_k,ck,dk,ek,fk) *  modeshape(k,Omega_k,ck,dk,ek,fk)
    intd2(k,Omega_k,ck,dk,ek,fk): integration of modeshaped2(k,Omega_k,ck,dk,ek,fk) *  modeshape(k,Omega_k,ck,dk,ek,fk)
    intd1(k,Omega_k,ck,dk,ek,fk): integration of modeshaped(k,Omega_k,ck,dk,ek,fk)  *  modeshape(k,Omega_k,ck,dk,ek,fk)
"""
def intd4(k,Omega_k,ck,dk,ek,fk):
    return(modeshaped4(k,Omega_k,ck,dk,ek,fk)*modeshape(k,Omega_k,ck,dk,ek,fk))

I1 = quad_vec(intd4, 0, lx, args=(Omega_m,cx,dx,ex,fx),epsabs=1.49e-12, epsrel=1.49e-12,limit=5000000)[0]
I5 = quad_vec(intd4, 0, ly, args=(Omega_n,cy,dy,ey,fy),epsabs=1.49e-12, epsrel=1.49e-12,limit=5000000)[0]

def intd2(x,km,cx,dx,ex,fx):
    return(modeshaped2(x,km,cx,dx,ex,fx)*modeshape(x,km,cx,dx,ex,fx))
I3 = quad_vec(intd2, 0, lx, args=(Omega_m,cx,dx,ex,fx),epsabs=1.49e-12, epsrel=1.49e-12,limit=5000000)[0]
I4 = quad_vec(intd2, 0, ly, args=(Omega_n,cy,dy,ey,fy),epsabs=1.49e-12, epsrel=1.49e-12,limit=5000000)[0]

def intd1(x,km,cx,dx,ex,fx):
    return(modeshape(x,km,cx,dx,ex,fx)*modeshape(x,km,cx,dx,ex,fx))
I6 = quad_vec(intd1, 0, lx, args=(Omega_m,cx,dx,ex,fx),epsabs=1.49e-12, epsrel=1.49e-12,limit=5000000)[0] 
I2 = quad_vec(intd1, 0, ly, args=(Omega_n,cy,dy,ey,fy),epsabs=1.49e-12, epsrel=1.49e-12,limit=5000000)[0] 

dof = 2*n.size*m.size # number of total dof i.e. for a system of two coupled equation dof in x direction * dof in y direction * 2

#%% Matrix of spacial discretization 
I2I6 = (I2[None,:]*I6[:,None]).reshape(4,)*np.identity(dof//2)
IM2 = (I2[None,:]*I3[:,None]+I4[None,:]*I6[:,None]).reshape(4,)*np.identity(dof//2)
IM1 = (I2[None,:]*I1[:,None]+2*I4[None,:]*I3[:,None]+I5[None,:]*I6[:,None]).reshape(4,)*np.identity(dof//2)
#%% 
"""
Calculation of homogenous macroscopic parameters:
        sigma: the airflow resistivity
        phi_tot: the total perforation ratio
        alpha_inf: the corrected tortuosity which takes into account the air flow distortion and the interaction between the perforations.
        E and D: respectivly the Young modulus and the bending stifness
"""
sigma1 = 32*eta/(phi1*d1**2); sigma2 = 32*eta/(phi2*d2**2); # sigma1 and sigma2 the airflow resistivity of the subdomain 1 and 2
sigma = 1/(1/sigma1+1/sigma2) # the global airflow resistivity

phi_tot = phi1 + phi2 # the total perforation ratio

B1 = 0.48/h*np.sqrt(np.pi*d1**2) #sudomain 1
B2 = 0.48/h*np.sqrt(np.pi*d2**2) #sudomain 2
Btot = 1/phi_tot*(phi1*B1+phi2*B2)  #total
alpha_inf = 1 + Btot*(1-1.14*np.sqrt(phi_tot)) #corrected airflow resistivity

#-------------------------
E = E*(1-phi_tot)**2/(1+(2-3*nu)*phi_tot)       #Young's modulus corrected for total perforation ratio
D = E*h**3/(12*(1-nu**2)); rho = phi_tot*rhof+(1-phi_tot)*rhos # Bending stifness
#%%
"""
Each matrix correspond to mass, stifness or damping in the folowing coupled discret system of equation:

    K1 w_s(t) + Ms1 w_s"(t) + M1 w"(t) = 0
    K2 w_s(t) + Ms2 w_s"(t) + M2 w"(t) + C2 w'(t) = 0

where w_s is the solid motion and w(t) is the relative fluid-solid motion

the operator (.)" is the second order derivation as a function of time and (.)' is the first order derivation.
"""
Ms1 = h*rho*I2I6            
Ms2 = rhof*I2I6
M1 = h*rhof*I2I6
M2 = rhof*alpha_inf/phi_tot*I2I6
C2 = sigma*I2I6            

K1 = D*IM1
K2 = Bf*IM2
matZeros = np.zeros_like(M1)

# Write the equation as a matrix such that: matM z"(t) + matC z'(t) + matK z(t) = 0 where z = [w_s, w]

matM = np.block([[Ms1,M1],[Ms2,M2]])
matK = np.block([[K1,matZeros],[K2,matZeros]])
matC = np.block([[matZeros,matZeros],[matZeros,C2]])

matD = np.block([[np.zeros_like(matM),np.identity(matM.shape[0])],[-np.dot(inv(matM),matK),-np.dot(inv(matM),matC)]]) # matrix D

λi,Pp = linalg.eig(matD) # eigenvalues and eigenvector of D

"""
Eigenvalues corresponding to structure displacement are complex conjugates 
λi​=βi​±jγi​ where the imaginary part imag(λi)​=γi​=ωi​1−ζi2​​ is the natural frequency of the mode i for the damped system 
and the real part real(λi)​=βi​=−ζi​ωi​, the damping term involved in the exponential decrease of the mode, 
expressions in which ωi​ is the natural frequency of the undamped mode i and ζi​, the corresponding modal damping ratio.
"""

# =============================================================================
#        classification by ascending order of the values and corresponding eigenvectors     
# =============================================================================
Pp_ = np.zeros((Pp.shape[0]+1,Pp.shape[0]),dtype=complex)
Pp_[0,:]=λi.transpose()
Pp_[1:,:]=Pp
temp = np.lexsort((abs(Pp_[0, :].imag), ))
Pp = Pp_[1:,temp]

λi = λi[np.lexsort((abs(λi.imag), ))]
Lpreal = λi.real; Lpimag = λi.imag
mask = abs(Lpreal) > 1e-10; Lpreal = Lpreal*mask

mask = abs(Lpimag) > 1e-10; Lpimag = Lpimag*mask
λi = Lpreal+Lpimag*complex(0,1)

Ppr = np.empty_like(Pp,dtype = float);

Ppreal = Pp.real
Pimag = Pp.imag

# =============================================================================
#        numerical error removal
# =============================================================================
mask = abs(Ppreal) > 1e-10
Ppreal = Ppreal*mask

mask = abs(Pimag) > 1e-10
Pimag = Pimag*mask

Pp = Ppreal+Pimag*complex(0,1)
print(λi)

# =============================================================================
#             matrix of eigenvectors according to their real and imag parts
# =============================================================================
pas = 1
for i in range(Pp.shape[0]):
    if (Pp[:,i].imag !=0).any():
        if pas == 2:
            Ppr[:,i] = Pp[:,i-1].imag
            pas = 1
        else:
            pas = 2
            Ppr[:,i] = Pp[:,i].real
    else:
      Ppr[:,i] = Pp[:,i].real  
mask = abs(Ppr) > 1e-10
Ppr = Ppr*mask


Lpr = np.dot(inv(Ppr),np.dot(matD,Ppr)) #matrice réelle des valeurs propres diagonale par bloc de 2
# =============================================================================
#       numerical error removal
# =============================================================================   
mask_Lpr = abs(Lpr) > 1e-8
Lpr = Lpr*mask_Lpr