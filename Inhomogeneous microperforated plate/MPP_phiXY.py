# -*- coding: utf-8 -*-
"""
This Pyton script presents the modal analysis performed for the study of microperforated plates with spacial distribution of perforation

In this script, the modal analysis in the case of an MPP with a perforation ratio depending of the space
The inhomogenity function Ih(x,y) can be modified to maximize the added damping in a particular mode
"""
#%%
"""
  Import necessary packages
"""
import numpy as np  

from numpy.linalg import inv
from scipy.integrate import nquad
from scipy import linalg

from mode_shape import mode_shape
#%%
"""
Parameter dictionary

All necessary parameters are stored in the following dictionary. They are divided into 4 categories: 
    
    [1]: the number of dof in the discretization on plate modes and boundary conditions (S: simply supported, C: emasculated and F: free)
    [2]: the geometrical parameters of the structure;
    [3]: perforation parameters (diameter and perforation ratio);
    [4]: the mechanical parameters of the plate and the fluid parameters.
    
All parameters are given in SI units.
"""
param = {'m': np.array([1,2]), # number of mode in the x direction
         'n': np.array([1,2]), # number of mode in the y direction
         'clx' : 'SS',         # Boundary condition in the x direction
         'cly' : 'SS',         # Boundary condition in the y direction
         #
         'a': 490e-3,          # plate length in the x direction (m)
         'b': 570e-3,          # plate length in the y direction (m)
         'h': 1e-3,            # plate thickness (in the z direction) (m)
         #
         'phi':0.25,            # perforation ratio
         'd':1.4e-3,           # perforation diameter
         #
         'rhof': 1.213,        # fluid density (kg/m3)
         'eta': 1.84e-5,       # fluid dynamic viscosity (Pa/s)
         'rhos': 2680,         # solid density (kg/m3)
         'E': 66000000000,     # solid Young modulus (Pa)
         'nu': 0.33,           # Poisson coefficient
         'Bf': 1.3e5}          # fluid Blunck mudulus (Pa)
#%%
""" ---------- Recovery and assignment of dictionary parameters ----------- """
m = param['m']; clx = param['clx']; n = param['n']; cly = param['cly']
lx = param['a']; ly = param['b']; h = param['h']
phi = param['phi']; d = param['d']; 
rhof = param['rhof']; rhos = param['rhos']; eta = param['eta']; E = param['E']; nu = param['nu']; Bf = param['Bf']
#%% 
varsigma = 32*eta/(d**2);
B = 0.48/h*np.sqrt(np.pi*d**2)
#%%
"""
the function Ih(x,y) defines the normalized inhomogeneity function 
the function phi_(x,y) is the spatial perforation ratio 

the global perforation ratio phi_g is defined by a surface integral
"""
def Ih(x,y): # inhomogeneity function that can be modified by another spactial function according x and/or y
    return(np.sin(np.pi*x/lx)*np.sin(np.pi*y/ly))
def phi_(x,y):
    return(phi*Ih(x,y))

phi_g = phi/lx/ly*nquad(Ih,[[0, lx], [0, ly]])[0] #gobal perforation ration
"""
Calculation of mode shape according x or y direction via the function mode_shape(k,clk,lk)
Input: mode index, boudary condition, plate length

Output: Omega_k and the coefficients ck,dk,ek,fk
"""
Omega_m, cx, dx, ex, fx = mode_shape(m, clx, lx) # in x direction
Omega_n, cy, dy, ey, fy = mode_shape(n, cly, ly) # in y direction

#%%
"""
Each function calculated the derivative according x or y: 
    modeshape(k,Omega_k,ck,dk,ek,fk) : mode shape according the x or y direction
    modeshape2(k,Omega_k,ck,dk,ek,fk) : the second order derivative of modeshape(k,Omega_k,ck,dk,ek,fk) according to k
    modeshape4(k,Omega_k,ck,dk,ek,fk) : the fourth order derivative of modeshape(k,Omega_k,ck,dk,ek,fk) according to k
"""
def modeshape(k,Omega_k,ck,dk,ek,fk):
    return(ck*np.cosh(Omega_k*k) + dk*np.cos(Omega_k*k) + ek*np.sinh(Omega_k*k) +fk*np.sin(Omega_k*k))
def modeshaped2(k,Omega_k,ck,dk,ek,fk):
    return(Omega_k**2*(ck*np.cosh(Omega_k*k)-dk*np.cos(Omega_k*k)+ek*np.sinh(Omega_k*k)-fk*np.sin(Omega_k*k)))
def modeshaped4(k,Omega_k,ck,dk,ek,fk):
    return((Omega_k**4)*modeshape(k,Omega_k,ck,dk,ek,fk))
"""
Each function calculated the spacial integration, depending of spacial perforation ratio, due to the projection of plate equations on the non-perforated plate modes

    intd4(x, y,Omega_m,cx,dx,ex,fx,kn,cy,dy,ey,fy): integration of D(x,y) × ∇^4 Ψ(x,y) × transpose(Ψ(x,y))
    intd2(x, y,Omega_m,cx,dx,ex,fx,kn,cy,dy,ey,fy): integration of  ∇^2 Ψ(x,y) × transpose(Ψ(x,y))
    intd1(x, y,Omega_m,cx,dx,ex,fx,kn,cy,dy,ey,fy): integration of Ψ(x,y) × transpose(Ψ(x,y))
    intd1_phi(x, y,Omega_m,cx,dx,ex,fx,kn,cy,dy,ey,fy): integration of phi(x,y) × Ψ(x,y) × transpose(Ψ(x,y))
    intd1_alpha_inf(x, y,Omega_m,cx,dx,ex,fx,kn,cy,dy,ey,fy): integration of alpha_inf(x,y) ×  Ψ(x,y) × transpose(Ψ(x,y))
    
"""
def intd4(x, y,Omega_m,cx,dx,ex,fx,Omega_n,cy,dy,ey,fy):
    return((1-phi_(x,y))**2/(1+(2-3*nu)*phi_(x,y))
           * (modeshaped4(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(y,Omega_n,cy,dy,ey,fy)*modeshape(y,Omega_n,cy,dy,ey,fy)
           + 2*modeshaped2(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshaped2(y,Omega_n,cy,dy,ey,fy)*modeshape(y,Omega_n,cy,dy,ey,fy)
           + modeshaped4(y,Omega_n,cy,dy,ey,fy)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(y,Omega_n,cy,dy,ey,fy)))
def intd2(x, y,Omega_m,cx,dx,ex,fx,Omega_n,cy,dy,ey,fy):
    return(modeshaped2(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(y,Omega_n,cy,dy,ey,fy)*modeshape(y,Omega_n,cy,dy,ey,fy)
           +modeshaped2(y,Omega_n,cy,dy,ey,fy)*modeshape(y,Omega_n,cy,dy,ey,fy)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx))

def intd1(x,y,Omega_m,cx,dx,ex,fx,Omega_n,cy,dy,ey,fy):
    return(modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(y,Omega_n,cy,dy,ey,fy)*modeshape(y,Omega_n,cy,dy,ey,fy))
def intd1_phi(x,y,Omega_m,cx,dx,ex,fx,Omega_n,cy,dy,ey,fy):
    return(phi_(x,y)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(y,Omega_n,cy,dy,ey,fy)*modeshape(y,Omega_n,cy,dy,ey,fy))
def intd1_alpha_inf(x,y,Omega_m,cx,dx,ex,fx,Omega_n,cy,dy,ey,fy):
    return((1 + B -1.14*B*np.sqrt(phi_(x,y)))*
        modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(x,Omega_m,cx,dx,ex,fx)*modeshape(y,Omega_n,cy,dy,ey,fy)*modeshape(y,Omega_n,cy,dy,ey,fy))
#%% ------- Matrix of spacial discretization ------------ 
IM1 = np.empty((m.size,n.size));IM2 = np.empty_like(IM1);I2I6_phi = np.empty_like(IM1);
I2I6_alpha_inf = np.empty_like(IM1); I2I6 = np.empty_like(IM1);
for i in range(m.size):
    for j in range(n.size):   
        IM1[i,j] = nquad(intd4, [[0, lx], [0, ly]], args=(Omega_m[i],cx,dx,ex[i],fx[i],Omega_n[j],cy,dy,ey[j],fy[j],),opts={"points": [-5, 5],'limit' : 5000000})[0]
        IM2[i,j] = nquad(intd2, [[0, lx], [0, ly]], args=(Omega_m[i],cx,dx,ex[i],fx[i],Omega_n[j],cy,dy,ey[j],fy[j],),opts={"points": [-5, 5],'limit' : 5000000})[0]
        I2I6_phi[i,j] = nquad(intd1_phi, [[0, lx], [0, ly]], args=(Omega_m[i],cx,dx,ex[i],fx[i],Omega_n[j],cy,dy,ey[j],fy[j],),opts={"points": [-5, 5],'limit' : 5000000})[0]
        I2I6_alpha_inf[i,j] = nquad(intd1_alpha_inf, [[0, lx], [0, ly]], args=(Omega_m[i],cx,dx,ex[i],fx[i],Omega_n[j],cy,dy,ey[j],fy[j],),opts={"points": [-5, 5],'limit' : 5000000})[0]
        I2I6[i,j] = nquad(intd1, [[0, lx], [0, ly]], args=(Omega_m[i],cx,dx,ex[i],fx[i],Omega_n[j],cy,dy,ey[j],fy[j],),opts={"points": [-5, 5],'limit' : 5000000})[0]
#%%
dof = 2*n.size*m.size
"""
Each matrix correspond to mass, stifness or damping in the folowing coupled discret system of equation:

    K1 w_s(t) + Ms1 w_s"(t) + Mf1 w_f"(t) = 0
    K2 w_s(t) + Ms2 w_s"(t) + Mf2 w_f"(t) + Cs2 w_s'(t) + Cf2 w_f'(t) = 0

where w_s is the solid motion and w_f(t) is the relative fluid motion

the operator (.)" is the second order derivation as a function of time and (.)' is the first order derivation.
"""
IM1 = IM1.reshape(4,)*np.identity(dof//2); IM2 = IM2.reshape(4,)*np.identity(dof//2);
I2I6 = I2I6.reshape(4,)*np.identity(dof//2); I2I6_phi = I2I6_phi.reshape(4,)*np.identity(dof//2);
I2I6_alpha_inf = I2I6_alpha_inf.reshape(4,)*np.identity(dof//2);

Ms1 = h*rhos*(I2I6 - I2I6_phi)
Ms2 = rhof*(I2I6-I2I6_alpha_inf)
Mf1 = h*rhof*I2I6_phi
Mf2 = rhof*I2I6_alpha_inf
Cs2 = -varsigma*I2I6            
Cf2 = varsigma*I2I6   

K1 = E*h**3/12/(1-nu**2)*IM1
K2 = Bf*IM2
matZeros = np.zeros_like(Ms1)

# Write the equation as a matrix such that: matM z"(t) + matC z'(t) + matK z(t) = 0 where z = [w_s, w]

matM = np.block([[Ms1,Mf1],[Ms2,Mf2]])
matK = np.block([[K1,matZeros],[K2,matZeros]])
matC = np.block([[matZeros,matZeros],[Cs2,Cf2]])

# Resulting D matrix
matD = np.block([[np.zeros_like(matM),np.identity(matM.shape[0])],[-np.dot(inv(matM),matK),-np.dot(inv(matM),matC)]]) 

"""
Eigenvalues corresponding to structure displacement are complex conjugates 
λi​=βi​±jγi​ where the imaginary part imag(λi)​=γi​=ωi​1−ζi2​​ is the natural frequency of the mode i for the damped system 
and the real part real(λi)​=βi​=−ζi​ωi​, the damping term involved in the exponential decrease of the mode, 
expressions in which ωi​ is the natural frequency of the undamped mode i and ζi​, the corresponding modal damping ratio.
"""

λi,Pp = linalg.eig(matD) # eigenvalues and eigenvector of D
print(λi)


