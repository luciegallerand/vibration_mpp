# -*- coding: utf-8 -*-
"""

This function calculated the mode shape according x or y direction

#----------------------------------------------

Decoupled functions :

phi(x) = c*cosh(kx) + d*cos(kx) + e*sinh(kx) + d*sin(kx)

% Boundary conditions : 
%
% % % F --> d²phi/dx²(0,t) = 0 and d^3phi/dx^3(0,t) = 0
% % % S --> phi(0,t) = 0 and d²phi/dx²(0,t) = 0
% % C --> phi(0,t) = 0 and dphi/dx(0,t) = 0

to obtain the characteristic equation we calculate the determinant of A (matrix of the boundary conditions)

resolution of the equations 4 unknowns for the constants

#------------------------------------------------
"""

from scipy.optimize import newton
import numpy as np
def mode_shape(k,cl,Lk):    
    def f(x):
        if cl == 'SS':
            return np.sin(x)
        if cl == 'CC' or cl == 'FF':
            return np.cos(x)*np.cosh(x)-1            
        if cl == 'FC' or cl == 'CF':
            return np.cos(x)*np.cosh(x)+1
        else:
            return np.tan(x)-np.tanh(x)              
    x0 = np.empty_like(k,dtype=float)    
    for i in range(0,k.size):
        if cl=='CC' or cl == 'FF':
            x = (k[i]+1)*np.pi
        else:
            x = k[i]*np.pi       
        x0[i] = newton(f, x, fprime=None, args=(), tol=1.48e-14, maxiter=50, fprime2=None)
    if cl == 'FF':
        i = k.size - 1
        while i-2 >= 0:
            x0[i] = x0[i-2]
            i -= 1
        x0[0] = 0; x0[1] = 0;
        
    def constants(cl,LkOmega_k):
        if cl == 'SS':
            c = 0; d = 0; e = np.zeros_like(LkOmega_k,dtype = np.float64); f = np.ones_like(LkOmega_k,dtype=np.float64);
        elif cl == 'CC':
            c = 1; d = -c; e = -(np.cosh(LkOmega_k)-np.cos(LkOmega_k))/(np.sinh(LkOmega_k)-np.sin(LkOmega_k)); f = -e;
        elif cl == 'CF' or cl == 'FC':
            c = 1; d = -c; e = -(np.cosh(LkOmega_k)+np.cos(LkOmega_k))/(np.sinh(LkOmega_k)+np.sin(LkOmega_k)); f = e;
        elif cl == 'SC' or cl == 'CS':
            c = 1; d = c; e = -(np.cosh(LkOmega_k)+np.cos(LkOmega_k))/(np.sinh(LkOmega_k)+np.sin(LkOmega_k)); f = e;
        elif cl == 'FS' or cl == 'SF':
            c = 1; d = -c; e = -(np.cosh(LkOmega_k)-np.cos(LkOmega_k))/(np.sinh(LkOmega_k)-np.sin(LkOmega_k)); f = -e;
        elif cl == 'FF':
            c = 1; d = c; e = np.empty_like(LkOmega_k,dtype = np.float64);
            e[:2] = 0; e[2:] = -(np.cosh(LkOmega_k[2:]) - np.cos(LkOmega_k[2:]))/(np.sinh(LkOmega_k[2:]) - np.sin(LkOmega_k[2:])); f = e                      
        return c, d, e, f 
    c, d, e, f = constants(cl, x0)
    Omega_k = x0/Lk  
    return Omega_k, c, d, e, f