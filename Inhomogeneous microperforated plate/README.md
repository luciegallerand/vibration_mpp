# Vibration of inhomogeneous microperforated plate (MPP)

<h5> Damping performance of finite microperforated plates using multi-sized
and spacial distributions of perforations <h5>

These Python scripts extend the jupyter page of the [this git repository](https://gitlab.com/luciegallerand/vibration_mpp/-/tree/main/Linear) by providing a modal analysis for MPPs that are not homogeneous in terms of perforation. Two designs corresponding to one script each are proposed:  
-  **`MPP_2perf.py`** proposes an MPP with multi-sized of perforation diameters. Here, the global macroscopic parameters are defined via a homogenization approach. For this purpose, N subdomains corresponding to the N perforation groups are defined and under the assumption of low interaction of the fluid domains. The macroscopic parameters are defined for each subdomain and then combined via homogenization approaches. The resulting equations are rewritten and solved via a modal approach. 

-  **`MPP_phiXY.py`** proposes an MPP with spatially inhomogeneous perforations. A space-dependent perforation ratio is defined from an inhomogeneity function Ih(x,y). The equations are rewritten and projected onto the eigenmodes of a non-perforated plate. A modal analysis is then performed  and the eigenfrequencies and viscous damping coefficients are obtained.

For each script, several boundary conditions can be chosen: 
- simply supported (S)
- free (F)
- clamped (C). 

These must be entered in pairs. Each of them corresponds to the boundary conditions on one axis (i.e. x or y for a plate in the xy plane). The  **`mode_shape`** function calculates the plate eigenmodes. 
The equations are then projected on this basis and the resulting integrals are numerically after a discretization in space of the plate. As a results the values and eigenvectors of the discretized problem are print.
