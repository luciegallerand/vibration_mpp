# Linear

<h5> Vibration and damping analysis of a thin finite-size microperforated plate <h5>

This Jupyter NoteBook calculates the natural frequencies and modal damping factors of a microperforated plate saturated by a light-density fluid via a modal approach. The plate is assumed to be thin so only bending vibrations are considered.
Two homogeneous phases i.e. fluid and solid whose mechanical parameters are given in input are considered.
The function **`get_MPPparam`** calculates the equivalent fluid parameters for the microperforated plate. Corrections to take into account the interactions between the perforations are also applied.

Several boundary conditions can be chosen: simply supported (S), free (F) and clamped (C). These must be entered in pairs. Each of them corresponds to the boundary conditions on one axis (i.e. x or y for a plate in the xy plane). The **`mode_shape`** function calculates the plate eigenmodes. 
The equations are then projected on this basis and the resulting integrals are computed with Legrendre polynomials after a discretization in space of the plate. The **`main`** function computes the values and eigenvectors of the discretized problem.

At the end, for each plate mode, the damping is plotted as a function of the perforation diameter as well as the resonant frequency. 

